# Results
- Precision and recall results for the classifier differ heavily by the dataset against which it is checked.
- Best results are found for PubMed (since it was trained on PubMed data). However, due to the different set sizes, the classifier is better in identifying "not review" (F1 = 0.96) than in identifying "reviews" (F1 = 0.68).
- Assuming that every item in a review journal is a review, the classifier is ok on false negatives (Recall = 0.7)
- Classifying 1000 Web of Science abstracts which are NOT in PubMed (naive set), the classifier performs low in identifying reviews (F1 = 0.18) but pretty good in identifying not reviews (F1 = 0.91). This is possibly due to the low number of 22 reviews in this set of 1000. For Scopus with the same criteria, the results are only slightly better (F1 = 0.23), while the amount of true reviews is almost twice (40 vs 22).
- Using a more balanced sampling of Web of Science and Scopus that ensures a more equal representation of disciplines, languages, and doctypes, the classifier performs better. While for reviews in Scopus it is F1 = 0.56 results are even better for the Web of Science sample (F1 = 0.81) in which there is an equal number of reviews and non reviews.

# Conclusion
- The classifier produces a substantial amount of false positives, which is greatly amplified by the low number of review articles in the overall databases (see table "overview"). This behavior affects all types of samples and becomes less worse the more reviews are in the set. For further adjustment, the specificity of the model might be increased (threshold?).
- Although the classifier performs better if balanced datasets are used for evaluation (higher number of reviews), it must be considered that such sets are biased and unusual. Since the goal of the classifier is to run it at large against OpenAlex, its performance should be optimized for this general scope.


**For comparison: Overview over rate of reviews of different databases/sets:**
|            | WoS        | Scopus     | WoS (excl. PubMed) | Scopus (excl. PubMed) | PubMed (sample)²| Semantic Scholar |
|------------|------------|------------|--------------------|-----------------------|-----------------|------------------|
| items      | 72,767,643 | 52,320,937 |         48,387,290 |            33,154,792 |      11,215,278 |      214,295,917 |
| not review |      96,7% |      92,2% |              98,6% |                 93,6% |           87,1% |            93,4% |
| review     |       3,3% |       7,8% |               1,4% |                  6,4% |           12,9% |             6,6% |
² based on dzhwaschniedermann.aa_master


# Data

## Web of SCience (naive)
Pubyear >= 2019, no pmid, N= 1000 by random

Crosstable of absolute matches
| orig_class     | notreview | review |
|----------------|-----------|--------|
| classifier     |           |        |
| notreview      | 823       | 4      |
| review         | 155       | 18     |

Precision & Recall

|              | precision | recall | f1-score | support |
|--------------|-----------|--------|----------|---------|
| notreview    | 1.00      | 0.84   | 0.91     | 978     |
| review       | 0.10      | 0.82   | 0.18     | 22      |
|              |           |        |          |         |
| accuracy     |           |        | 0.84     | 1000    |
| macro avg    | 0.55      | 0.83   | 0.55     | 1000    |
| weighted avg | 0.98      | 0.84   | 0.90     | 1000    |

## Scopus (naive)
Pubyear >= 2019, no pmid, N= 1000 by random

Crosstable of absolute matches
| orig_class | notreview | review |
|------------|-----------|--------|
| classifier |           |        |
| notreview  | 736       | 5      |
| review     | 224       | 35     |

Precision & Recall
|              | precision | recall | f1-score | support |
|--------------|-----------|--------|----------|---------|
| notreview    | 0.99      | 0.77   | 0.87     | 960     |
| review       | 0.14      | 0.88   | 0.23     | 40      |
|              |           |        |          |         |
| accuracy     |           |        | 0.77     | 1000    |
| macro avg    | 0.56      | 0.82   | 0.55     | 1000    |
| weighted avg | 0.96      | 0.77   | 0.84     | 1000    |

## Web of Science (stratified)
Pubyear >= 2019, no pmid, equal sets for fields and language 


Crosstable of absolute matches

| orig_class | notreview | review |
|------------|-----------|--------|
| classifier |           |        |
| notreview  | 214       | 35     |
| review     | 86        | 265    |


Precision & Recall

|              | precision | recall | f1-score | support |
|--------------|-----------|--------|----------|---------|
| notreview    | 0.86      | 0.71   | 0.78     | 300     |
| review       | 0.75      | 0.88   | 0.81     | 300     |
|              |           |        |          |         |
| accuracy     |           |        | 0.80     | 600     |
| macro avg    | 0.81      | 0.80   | 0.80     | 600     |
| weighted avg | 0.81      | 0.80   | 0.80     | 600     |

## Scopus (stratified)
Pubyear >= 2005, no pmid, equal sets for fields and language 


Crosstable of absolute matches
| orig_class | notreview | review |
|------------|-----------|--------|
| classifier |           |        |
| notreview  | 709       | 66     |
| review     | 281       | 221    |

Precision & Recall

|              | precision | recall | f1-score | support |
|--------------|-----------|--------|----------|---------|
| notreview    | 0.91      | 0.72   | 0.80     | 990     |
| review       | 0.44      | 0.77   | 0.56     | 287     |
|              |           |        |          |         |
| accuracy     |           |        | 0.73     | 1277    |
| macro avg    | 0.68      | 0.74   | 0.68     | 1277    |
| weighted avg | 0.81      | 0.73   | 0.75     | 1277    |

## Review Journals
Crosstable of absolute matches
| orig_class   | review |   |
|--------------|--------|---|
| mikesclasses |        |   |
| notreview    | 304    |   |
| review       | 696    |   |

Precision & Recall
|              | precision | recall | f1-score | support |
|--------------|-----------|--------|----------|---------|
| notreview    | 0.00      | 0.00   | 0.00     | 0       |
| review       | 1.00      | 0.70   | 0.82     | 1000    |
|              |           |        |          |         |
| accuracy     |           |        | 0.70     | 1000    |
| macro avg    | 0.50      | 0.35   | 0.41     | 1000    |
| weighted avg | 1.00      | 0.70   | 0.82     | 1000    |

## PubMed (for checkup)
Crosstable of absolute matches

| orig_class   | notreview | review |
|--------------|-----------|--------|
| classifier   |           |        |
| notreview    | 855       | 15     |
| review       | 55        | 75     |

Precision & Recall
|              | precision | recall | f1-score | support |
|--------------|-----------|--------|----------|---------|
| notreview    | 0.98      | 0.94   | 0.96     | 910     |
| review       | 0.58      | 0.83   | 0.68     | 90      |
|              |           |        |          |         |
| accuracy     |           |        | 0.93     | 1000    |
| macro avg    | 0.78      | 0.89   | 0.82     | 1000    |
| weighted avg | 0.95      | 0.93   | 0.94     | 1000    |

## Semantic Scholar 
Pubyear >= 2019, no pmid, N= 1000 by random

Crosstable of absolute matches

| orig_class   | notreview | review |
|--------------|-----------|--------|
| classifier   |           |        |
| notreview    | 686       | 39     |
| review       | 216       | 59     |

Precision & Recall
|              | precision | recall | f1-score | support |
|--------------|-----------|--------|----------|---------|
| notreview    | 0.95      | 0.76   | 0.84     | 902     |
| review       | 0.21      | 0.60   | 0.32     | 98      |
|              |           |        |          |         |
| accuracy     |           |        | 0.74     | 1000    |
| macro avg    | 0.58      | 0.68   | 0.58     | 1000    |
| weighted avg | 0.87      | 0.74   | 0.79     | 1000    |

# SQL queries

## Web of Science (naive)

```
SELECT 
	aa.doi, 
	unnest(cc.abstract) abstract,
	case 
		when 'Review' = any(aa.item_type) then 'review'
	else 'notreview'
	end orig_class
FROM wos_b_202310.items aa
left join wos_b_202310.abstracts cc on aa.item_id = cc.item_id
where aa.pmid is null and cc.abstract is not null and aa.pubyear >= 2019
order by random()
limit 1000;
```

## Scopus (naive)

```
SELECT 
	aa.doi, 
	unnest(cc.abstract) abstract,
	case 
		when 'Review' = any(aa.item_type) then 'review'
	else 'notreview'
	end orig_class
FROM scp_b_202310.items aa
left join scp_b_202310.abstracts cc on aa.item_id = cc.item_id
where aa.pmid is null and cc.abstract is not null and aa.pubyear >= 2019
order by random()
limit 1000;
```

## Web of Science (stratified)

```
WITH sample_by_discipline_language_doctype AS (
	SELECT oecd_description,
		language,
        item_id,
		doi,
		abstract,
		orig_class,
        row_number() OVER(PARTITION BY oecd_description, language, orig_class ORDER BY random()) AS random_sort
    FROM (
    	SELECT DISTINCT it.item_id,
			it.doi,
			oecdf.oecd_description,
			abstract,
			orig_class,
			CASE
				WHEN language = 'eng'
					THEN 'eng'
				ELSE 'non-eng'
			END AS language
    	FROM dzhwsstahlschmidt.oecd_fields_wos202304 oecdf
    	LEFT JOIN (
			SELECT item_id, doi, field, UNNEST(languages) AS language, orig_class
			FROM(
	    		SELECT item_id,
					doi,
					UNNEST(class_name) AS field,
					languages,
					CASE 
						WHEN 'Review' = any(item_type)
							THEN 'review'
						ELSE 'notreview'
					END orig_class
    			FROM wos_b_202304.items
    			WHERE pubyear >= 2019
					AND pmid IS NULL
			) tmp
    	) it
        	ON UPPER(oecdf.vendor_description) = UPPER(it.field)
		JOIN wos_b_202304.abstracts abs
			ON abs.item_id = it.item_id
			AND abstract IS NOT NULL
    ) tmp
)
SELECT oecd_description,
	language,
	orig_class,
	item_id,
	doi,
	abstract
FROM sample_by_discipline_language_doctype
WHERE random_sort <= 25;

```

## Scopus (stratified)

```
WITH sample_by_discipline_language_doctype AS (
	SELECT oecd_description,
		language,
        item_id,
		doi,
		abstract,
		orig_class,
        row_number() OVER(PARTITION BY oecd_description, language, orig_class ORDER BY random()) AS random_sort
    FROM (
    	SELECT DISTINCT it.item_id,
			it.doi,
			oecdf.oecd_description,
			abstract,
			orig_class,
			CASE
				WHEN language = 'eng'
					THEN 'eng'
				ELSE 'non-eng'
			END AS language
    	FROM dzhwsstahlschmidt.oecd_fields_scp202304 oecdf
    	LEFT JOIN (
			SELECT item_id, doi, field, UNNEST(languages) AS language, orig_class
			FROM(
	    		SELECT item_id,
					doi,
					UNNEST(class_name) AS field,
					languages,
					CASE 
						WHEN 'Review' = any(item_type)
							THEN 'review'
						ELSE 'notreview'
					END orig_class
    			FROM scp_b_202304.items
    			WHERE pubyear >= 2019
					AND pmid IS NULL
				LIMIT 1000
			) tmp
    	) it
        	ON UPPER(oecdf.vendor_description) = UPPER(it.field)
		JOIN scp_b_202304.abstracts abs
			ON abs.item_id = it.item_id
			AND abstract IS NOT NULL
    ) tmp
)
SELECT oecd_description,
	language,
	orig_class,
	item_id,
	doi,
	abstract
FROM sample_by_discipline_language_doctype
WHERE random_sort <= 75;

```

## Review Journals

```
--This sampling query draws 1000 random publications from review journals which are defined by having at least 1000 publications of which at least 90% are labeled as Reviews in Web of Science
SELECT 
	cc.doi, 
	unnest(bb.abstract) abstract,
	'review' orig_class
FROM (select source_title
	from wos_b_202310.items
	group by source_title 
	having count(item_id) >= 1000 
		and (nullif(count(case when 'Review' = any(item_type) then item_id else null end)::numeric,0) / nullif(count(item_id)::numeric,0)) >= 0.9) aa
left join wos_b_202310.items cc on aa.source_title = cc.source_title
left join wos_b_202310.abstracts bb on cc.item_id = bb.item_id
where bb.abstract is not null
--order by random()
limit 1000;
```

## PubMed (for crosscheck)

```
with classified_by_medline as (
	select aa.doi, unnest(bb.abstract) abstract, aa.orig_type orig_class
		from
			(select item_id, doi,
			case when min_rank in ('Review','SR','SR_Cochrane','SR_PRISMA_2009','SR_PRISMA_2020','SR_PRISMA_1999') then 'review' else 'notreview' end orig_type
			from aa_master order by random() limit 1500) aa
		left join scp_b_202310.abstracts bb on aa.item_id=bb.item_id
	)
select * from classified_by_medline
where abstract is not null 
limit 1000
```

## Semantic Scholar

```
SELECT externalids.DOI AS doi, abstract, 
		CASE 
			WHEN 'Review' IN UNNEST(publicationtypes) THEN 'review'
			ELSE 'notreview'
		END AS orig_class
FROM subugoe-wag-closed.S2AG.papers_2023_09_26 AS s2_papers
LEFT JOIN subugoe-wag-closed.S2AG.abstracts_2023_09_26 AS s2_abstracts
	ON s2_papers.corpusid = s2_abstracts.corpusid
WHERE abstract IS NOT NULL AND year >= 2019 AND externalids.DOI IS NOT NULL AND externalids.PubMed IS NULL AND externalids.PubMedCentral IS NULL
ORDER BY RAND()
LIMIT 1000
```
