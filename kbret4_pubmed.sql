with classified_by_medline as (
	select aa.doi, unnest(bb.abstract) abstract, aa.orig_type orig_class
		from
			(select item_id, doi,
			case when min_rank in ('Review','SR','SR_Cochrane','SR_PRISMA_2009','SR_PRISMA_2020','SR_PRISMA_1999') then 'review' else 'notreview' end orig_type
			from aa_master order by random() limit 1500) aa
		left join scp_b_202310.abstracts bb on aa.item_id=bb.item_id
	)
select * from classified_by_medline
where abstract is not null 
limit 1000