import psycopg2

# Database Credentials
DB_HOST = "biblio-p-db03"
DB_NAME = "kbprod"
DB_USER = "yourkbaccess"
DB_PASS = "yourpassword"

def database_credentials():
    return psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)
