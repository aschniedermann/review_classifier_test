--This sampling query draws 1000 random publications from review journals which are defined by having at least 1000 publications of which at least 90% are labeled as Reviews in Web of Science
SELECT 
	cc.doi, 
	unnest(bb.abstract) abstract,
	'review' orig_class
FROM (select source_title
	from wos_b_202310.items
	group by source_title 
	having count(item_id) >= 1000 
		and (nullif(count(case when 'Review' = any(item_type) then item_id else null end)::numeric,0) / nullif(count(item_id)::numeric,0)) >= 0.9) aa
left join wos_b_202310.items cc on aa.source_title = cc.source_title
left join wos_b_202310.abstracts bb on cc.item_id = bb.item_id
where bb.abstract is not null
--order by random()
limit 1000;