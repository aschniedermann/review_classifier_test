WITH sample_by_discipline_language_doctype AS (
	SELECT oecd_description,
		language,
        item_id,
		doi,
		abstract,
		orig_class,
        row_number() OVER(PARTITION BY oecd_description, language, orig_class ORDER BY random()) AS random_sort
    FROM (
    	SELECT DISTINCT it.item_id,
			it.doi,
			oecdf.oecd_description,
			abstract,
			orig_class,
			CASE
				WHEN language = 'eng'
					THEN 'eng'
				ELSE 'non-eng'
			END AS language
    	FROM dzhwsstahlschmidt.oecd_fields_scp202304 oecdf
    	LEFT JOIN (
			SELECT item_id, doi, field, UNNEST(languages) AS language, orig_class
			FROM(
	    		SELECT item_id,
					doi,
					UNNEST(class_name) AS field,
					languages,
					CASE 
						WHEN 'Review' = any(item_type)
							THEN 'review'
						ELSE 'notreview'
					END orig_class
    			FROM scp_b_202304.items
    			WHERE pubyear >= 2005
					AND pmid IS NULL
				LIMIT 10000
			) tmp
    	) it
        	ON UPPER(oecdf.vendor_description) = UPPER(it.field)
		JOIN scp_b_202304.abstracts abs
			ON abs.item_id = it.item_id
			AND abstract IS NOT NULL
    ) tmp
)
SELECT oecd_description,
	language,
	orig_class,
	item_id,
	doi,
	unnest(abstract) abstract
FROM sample_by_discipline_language_doctype
WHERE random_sort <= 100;
