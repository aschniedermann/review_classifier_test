# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 07:24:56 2024

@author: schniedermann
"""

import transformers
from setfit import SetFitModel
import psycopg2
import pandas
import sklearn
#from google.cloud import bigquery

# Load KB connecion info (see databasecredsDUMMY.py to add your info)
from databasecreds import database_credentials

# Download classifier model from the 🤗 Hub
model = SetFitModel.from_pretrained("almugabo/review_classifier")

def getDataFromKB(query):
    try:
        connection = database_credentials()
        cur = connection.cursor()
        print("waiting for data...")
        cur.execute(query)
        columnnames = [desc[0] for desc in cur.description]
        results = cur.fetchall()
        resultsframe = pandas.DataFrame(results, columns=columnnames)
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e) 
    finally:
        cur.close()
        try:
            return resultsframe
        except:
            pass

#def getDataFromBQ(query):
#    client = bigquery.Client(project='subugoe-collaborative')
#    return client.query(query).to_dataframe()


def applyClassifier(string):
    modeloutput = model(string)
    if modeloutput == True:
        result = "review"
    elif modeloutput == False:
        result = "notreview"
    return result


def main(queryname, query, database):
    
    # pull data from database
    if database == "KB":
        resultsframe = getDataFromKB(query)
    #if database == "BQ":
    #    resultsframe = getDataFromBQ(query)

    savepath = "./"+queryname+".pkl"
    #resultsframe.to_pickle(savepath)
    
    # apply classifier on abstracts texts
    classifierresults = resultsframe["abstract"].apply(applyClassifier)
    resultsframe["mikesclasses"] = classifierresults
    
    # create absolute crosstables
    crosstabbed = pandas.crosstab(resultsframe.mikesclasses, resultsframe.orig_class)
    print("Crosstable of absolute matches:"+queryname, end='\n')
    print(crosstabbed, end='\n')

    # calculate precision and recall
    mikesarray = resultsframe[["mikesclasses"]].to_numpy()
    origarray = resultsframe[["orig_class"]].to_numpy()
    pr_metrics = sklearn.metrics.classification_report(origarray,mikesarray)
    print("Precision & Recall:"+queryname, end='\n')
    print(pr_metrics)



query_wos = open("kbret1_wos.sql", "r").read()
query_scp = open("kbret2_scp.sql", "r").read()
query_rj = open("kbret3_rj.sql", "r").read()
query_pmid = open("kbret4_pubmed.sql", "r").read()
query_scp_nv = open("kbret5_scp_naive.sql", "r").read()
query_s2 = open("bqret1_s2.sql", "r").read()

#main("A_WebOfScience", query_wos, "KB")
#main("B_Scopus", query_scp, "KB")
#main("C_ReviewJournals", query_rj, "KB")
#main("D_PubMed", query_pmid, "KB")
#main("scp_nv", query_scp_nv, "KB")
#main("S2", s2_query, "BQ")

